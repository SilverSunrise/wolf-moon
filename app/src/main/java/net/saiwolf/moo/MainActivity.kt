package net.saiwolf.moo

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var timerMoney: CountDownTimer
    lateinit var timerFood: CountDownTimer
    lateinit var timerMood: CountDownTimer

    companion object {
        var indexFood = 100
        var indexMood = 100
        var countBone = 0
        var countToy = 0
        var money = 0
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        shop()
        currency()
        updatedMood()
        updateItems()
    }

    private fun updateItems() {
        ib_use_bone.setOnClickListener {
            if (indexFood + 11 <= 100 && countBone > 0) {
                indexFood += 11
                countBone -= 1
            }
        }

        ib_use_toy.setOnClickListener {
            if (indexMood + 9 <= 100 && countToy > 0) {
                indexMood += 9
                countToy -= 1
            }
        }
    }

    private fun updatedMood() {
        timerMood()
        timerFood()
    }

    private fun timerFood() {
        timerFood = object : CountDownTimer(Long.MAX_VALUE, 8000) {
            override fun onTick(millisUntilFinished: Long) {
                indexFood -= 1
            }

            override fun onFinish() {
            }
        }
        timerFood.start()
    }

    private fun timerMood() {
        timerMood = object : CountDownTimer(Long.MAX_VALUE, 4000) {
            override fun onTick(millisUntilFinished: Long) {
                indexMood -= 1
            }

            override fun onFinish() {
            }
        }
        timerMood.start()
    }

    private fun currency() {
        timerMoney = object : CountDownTimer(Long.MAX_VALUE, 1000) {
            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                money += 1
                tv_count_bone.text = countBone.toString()
                tv_count_toy.text = countToy.toString()
                tv_money.text = money.toString()
                tv_food.text = "$indexFood%"
                tv_heart.text = "$indexMood%"
            }

            override fun onFinish() {
            }
        }
        timerMoney.start()
    }

    private fun shop() {
        btn_shop_bone_buy.setOnClickListener {
            if (money >= 15) {
                money -= 15
                countBone += 1
            } else {
                Toast.makeText(this, "Not enough money!", Toast.LENGTH_SHORT).show()
            }
        }

        btn_shop_toy_buy.setOnClickListener {
            if (money >= 12) {
                money -= 12
                countToy += 1
            } else {
                Toast.makeText(this, "Not enough money!", Toast.LENGTH_SHORT).show()
            }
        }



        btn_shop.setOnClickListener {
            if (cl_shop.visibility == View.VISIBLE) {
                cl_shop.visibility = View.GONE
            } else {
                cl_shop.visibility = View.VISIBLE
            }

        }

        iv_shop_exit.setOnClickListener {
            cl_shop.visibility = View.GONE
        }
    }
}